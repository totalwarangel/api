<?php

use XSLT\API\Classes\Controller\Page\Impl\PageControllerIndexPageImpl;
use XSLT\API\Classes\View\Html\TwigEnviormentWrapper;

// Defines the root of this project just to keep it portable
define('PROJECT_ROOT', '');

// Autoloading
require_once PROJECT_ROOT . 'vendor/autoload.php';

// Renderer
$renderer = new TwigEnviormentWrapper();
$renderer->setTemplate('test.twig');

// Run the controller
$page = new PageControllerIndexPageImpl();
$page->bindRenderer($renderer);
$page->run();