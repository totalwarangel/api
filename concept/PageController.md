# Description

The page controller is in charge of:
* Calling the page rendering process
* Perform operations for the current page
* Delegate operations to other controllers

So the controller basically needs an renderer service for the page and maybe
other services (that can be controllers) to do the work for him.

# Methods

* public run() - Main method
* public ... - Optional things the controller can do
* private render() - To render the page
* private ... - Anything hidden from public the controller needs to run