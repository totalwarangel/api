<?php

namespace XSLT\API\Classes\Controller\Page\Impl;

use XSLT\API\Classes\Controller\Page\PageControllerInterface;

/**
 *
 * @author martin
 *
 */
class PageControllerIndexPageImpl implements PageControllerInterface
{
    /**
     *
     * @var TwigEnviormentWrapper
     */
    private $renderer;

    /**
     *
     * {@inheritDoc}
     * @see \Classes\Controller\Page\PageControllerInterface::bindRenderer()
     */
    public function bindRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Classes\Controller\Page\PageControllerInterface::post()
     */
    public function post()
    {
    }

    /**
     *
     * {@inheritDoc}
     * @see \Classes\Controller\Page\PageControllerInterface::get()
     */
    public function get()
    {
    }

    /**
     *
     * {@inheritDoc}
     * @see \Classes\Controller\Page\PageControllerInterface::run()
     */
    public function run()
    {
        $this->renderer->display();
    }
}

