<?php

namespace XSLT\API\Classes\Controller\Page;

/**
 * Basic interface for page controllers.
 * @author martin
 */
interface PageControllerInterface
{
    /**
     * Runs the page.
     */
    public function run();

    /**
     * Callback on get request to the page.
     */
    public function get();

    /**
     * Callback on posting something to the page.
     */
    public function post();

    /**
     * Binds an rendering service to the controller
     *
     * @param object $renderer
     */
    public function bindRenderer($renderer);
}

