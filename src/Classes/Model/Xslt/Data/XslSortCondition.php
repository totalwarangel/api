<?php

namespace XSLT\API\Classes\Model\Xslt\Data;

/**
 * Representation of a sort condition.
 * @author martin
 */
class XslSortCondition
{
    /**
     * Column to sort by
     * @var string
     */
    private $column;

    /**
     * Sorting mode
     * * 0: a > z
     * * 1: a < z
     * @var integer
     */
    private $mode;

    /**
     * Position mark in document
     * @var string
     */
    private $token;

    /**
     * Creates an sort condition object.
     *
     * @param string $token Marker in the document
     * @param string $column Name of column to sort by
     * @param integer $mode Sorting mode
     */
    public function __construct($token, $column, $mode)
    {
        $this->column = $column;
        $this->mode = $mode;
        $this->token = $token;
    }

    /**
     * Returns the column tag name.
     *
     * @return string Column
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * Returns the mode id.
     *
     * @return number Mode
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Returns the placement toten.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

}

