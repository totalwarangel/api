<?php

namespace XSLT\API\Classes\Model\Xslt\Data;

/**
 *
 * @author martin
 */
class XslFilterCondition
{
    /**
     * Position in the document
     * @var string
     */
    private $token;

    /**
     * First operand
     * @var mixed
     */
    private $operand1;

    /**
     * Second operand
     * @var mixed
     */
    private $operand2;

    /**
     * Operator
     * @var string
     */
    private $operator;

    /**
     * Creates a filter condition
     *
     * @param string $token Position in document
     * @param mixed $operand1 First operand
     * @param string $operator Operaror
     * @param mixed $operand2 Second operand
     */
    public function __construct($token, $operand1, $operator, $operand2)
    {
        $this->token    = $token;
        $this->operand1 = $operand1;
        $this->operator = $operator;
        $this->operand2 = $operand2;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getOperand1()
    {
        return $this->operand1;
    }

    /**
     * @return mixed
     */
    public function getOperand2()
    {
        return $this->operand2;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Checks the term for equal amount of opening and closing braces.
     *
     * @param unknown $term Term
     *
     * @return boolean Braces count equal
     */
    private function check($term)
    {
        $open  = substr_count($term, '(');
        $close = substr_count($term, ')');
        return $open == $close;
    }

    /**
     * Returns the recursive parsed filter term.
     *
     * @return string Term
     */
    public function getTerm()
    {
        $operand1 = $this->operand1;
        if ($operand1 instanceof XslFilterCondition) {
            $operand1 = '(' . $operand1->getTerm() . ')';
        }

        $operand2 = $this->operand2;
        if ($operand2 instanceof XslFilterCondition) {
            $operand2 = '(' . $operand1->getTerm() . ')';
        }

        $term = sprintf($this->operator, $operand1, $operand2);
        if (!$this->check($term)) {
            throw new \Exception('Wrong term: ' . $term);
        }
        return $term;
    }
}

