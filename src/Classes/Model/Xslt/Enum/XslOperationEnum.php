<?php

namespace XSLT\API\Classes\Model\Xslt\Enum;

/**
 * Defines tokens for statements
 * @author martin
 */
class XslOperationEnum
{
    /**
     * AND operation
     * @var string
     */
    const AND = '%s and %s';

    /**
     * OR operation
     * @var string
     */
    const OR = '%s or %s';

    /**
     * NOT operation
     * @var string
     */
    const NOT = 'not(%s)';

    /**
     * EQUALS operation
     * @var string
     */
    const EQUALITY = '%s = %s';

    /**
     * NOT EQUALS operation
     * @var string
     */
    const NOT_EQUAL = '%s != %s';

    /**
     * LESS THAN operation
     * @var string
     */
    const LESS_THAN = '%s &lt; %s';

    /**
     * LESS OR EQUAL operation
     * @var string
     */
    const LESS_OR_EQUAL = '%s &lt;= %s';

    /**
     * GREATER THAN operation
     * @var string
     */
    const GREATER_THAN = '%s &gt; %s';

    /**
     * GREATER OR EQUAL operation
     * @var string
     */
    const GREATER_OR_EQUAL = '%s &gt;= %s';
}

