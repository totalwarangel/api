<?php

namespace XSLT\API\Classes\Model\Xslt\Enum;

/**
 * Defines some control statement pattern for XSL files.
 * @author martin
 */
class XslStatementEnum
{
    /**
     * Begin of foreach declaration
     * @var string
     */
    const FOREACH_BEGIN = '<xsl:for-each select="%s">';

    /**
     * End of foreach declaration
     * @var string
     */
    const FOREACH_END = '</xsl:for-each>';

    /**
     * Begin of condition declaration
     * @var string
     */
    const IF_BEGIN = '<xsl:if test="%s">';

    /**
     * End of condition declaration
     * @var string
     */
    const IF_END = '</xsl:if>';

    /**
     * Sort declaration
     * @var string
     */
    const SORT = '<xsl:sort select="%s"/>';

    /**
     * Begin of template declaration
     * @var string
     */
    const TEMPLATE_BEGIN = '<xsl:template match="%s">';

    /**
     * End of template declaration
     * @var string
     */
    const TEMPLATE_END = '</xsl:template>';

    /**
     * Begin of stylesheet declaration
     * @var string
     */
    const STYLESHEET_BEGIN = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';

    /**
     * End of stylesheet declaration
     * @var string
     */
    const STYLESHEET_END = '</xsl:stylesheet>';
}

