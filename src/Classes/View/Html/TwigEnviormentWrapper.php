<?php

namespace XSLT\API\Classes\View\Html;

/**
 * Wrapps around Twig and holds template and arguments.
 * @author martin
 */
class TwigEnviormentWrapper
{
    /**
     * Twig enviorment
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * Template variables
     * @var array
     */
    private $args = array();

    /**
     * Path to template
     * @var string
     */
    private $template;

    /**
     * Constructor
     *
     * @param boolean $cache Use cache
     */
    public function __construct($cache = false)
    {
        $loader = new \Twig_Loader_Filesystem(PROJECT_ROOT . 'templates/twig');
        $this->twig = new \Twig_Environment($loader, array(
            'cache' => $cache
        ));
    }

    /**
     * Returns the enviorment.
     *
     * @return \Twig_Environment Enviorment
     */
    public function getEnviorment()
    {
        return $this->twig;
    }

    /**
     * Adds an parameter to the parameter arrey. The parameters will be used
     * for rendering the template.
     *
     * @param string $key Key
     * @param string $val Value
     */
    public function addParam($key, $val)
    {
        $this->args[$key] = $val;
    }

    /**
     * Sets the path to the Template.
     *
     * @param string $tpl
     */
    public function setTemplate($tpl)
    {
        $this->template = $tpl;
    }

    /**
     * Renders the page into a string.
     *
     * @return string
     */
    public function render()
    {
        if (!file_exists(PROJECT_ROOT . 'templates/twig/' . $this->template)) {
            throw new \Exception("Template '" . PROJECT_ROOT . "templates/twig/{$this->template}' not found!");
        }
        return $this->twig->render($this->template, $this->args);
    }

    /**
     * Displays the page.
     */
    public function display()
    {
        echo $this->render();
    }
}

