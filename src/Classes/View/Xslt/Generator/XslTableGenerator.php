<?php

namespace XSLT\API\Classes\View\Xslt\Generator;

use XSLT\API\Classes\Model\Xslt\Data\XslSortCondition;
use XSLT\API\Classes\Model\Xslt\Data\XslFilterCondition;

/**
 *
 * @author martin
 */
class XslTableGenerator
{

    /**
     * List of sort conditions
     * @var array
     */
    private $sortCondition = array();

    /**
     * List of filter conditions
     * @var array
     */
    private $filterCondition = array();

    /**
     * Adds a sort condition at the position.
     *
     * @param XslSortCondition $condition Condition
     */
    public function addSortCondition(XslSortCondition $condition)
    {
        array_push($this->sortCondition, $condition);
    }

    /**
     * Adds a filter condition at the position.
     *
     * @param XslFilterCondition $condition Condition
     */
    public function addFilterCondition(XslFilterCondition $condition)
    {
        array_push($this->filterCondition, $condition);
    }

    /**
     * Removes all sorting conditions.
     */
    public function clearSortConditions()
    {

    }

    /**
     * Removes all filtering conditions.
     */
    public function clearFilterConditions()
    {

    }

    /**
     * Saves the template under the target name.
     *
     * @param string $target File path
     */
    public function save($target)
    {

    }

    /**
     * Place all control statements inside the template.
     */
    private function build()
    {

    }

    /**
     *
     */
    private function placeSortConditions()
    {

    }

    /**
     *
     */
    private function placeFilterConditions()
    {

    }
}

